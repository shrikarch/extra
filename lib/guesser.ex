defmodule Extra.Guesser do
  def play do
    {message, state} = GenServer.call(ServerProcess, {:tell})
    IO.puts message
    guess(state)
  end

  def guess(state) do
    guessed = div(state.min + state.max, 2)
    # It is a binary search algorithm known to be of O(log(n)) time
    # Came across this: http://bit.ly/2eKSfds
    IO.puts("Is it #{guessed}?")
    GenServer.call(ServerProcess, {:get_guess, guessed})
    |> handle_decision(guessed, state)
  end

  def handle_decision(:high, guess, state) do
    Map.update!(state, :min, fn(x) -> guess+1 end)
    |> Map.update!(:moves, fn(x) -> state.moves+1 end)
    |> guess()
  end
  def handle_decision(:low, guess, state) do
    Map.update!(state, :max, fn(x) -> guess-1 end)
    |> Map.update!(:moves, fn(x) -> state.moves+1 end)
    |> guess()
  end
  def handle_decision(:correct, guess, state) do
     IO.puts "Yass! I won in #{state.moves} moves. The number was #{state.chosen}"
  end
end
