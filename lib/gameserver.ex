defmodule Extra.GameServer do
  use GenServer


  #############
  ##   API   ##
  #############
  def start_link(min, max) do
    GenServer.start_link(__MODULE__, [min, max], name: ServerProcess)
  end

  ########################
  ##   Implementation   ##
  ########################
  def init([min, max]) do
    { :ok, Enum.into([
      min: min,
      max: max,
      moves: 0,
      chosen: nil], %{}) }
  end

  def handle_call({:tell}, _from, state) do
    state = Map.update!(state, :chosen, fn(x) -> :rand.uniform(state.max) end)
    message = "I'm thinking of a number between #{state.min} and #{state.max}"
    {:reply, {message,state}, state}
  end
  def handle_call({:get_guess, guessed}, _from, state) do
    cond do
      guessed == state.chosen ->
        IO.puts("Yes")
        {:reply, :correct, state}
      guessed < state.chosen ->
        IO.puts("High")
        {:reply, :high, state}
      guessed > state.chosen ->
        IO.puts("Low")
        {:reply, :low, state}
    end
  end

end
