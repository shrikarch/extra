defmodule Sd do
  defmodule State do
    defstruct min: 1, max: 100, moves: 0
  end

  def play do
    state = %State{}
    IO.puts "Think of a num b/w #{state.min} and #{state.max}"
    guess(state)
  end

  def guess(state) do
    guess = div(state.min + state.max, 2)
    IO.gets("Is it #{guess}? yes/no: ")
    |> String.trim
    |> String.downcase
    |> handle_answer(guess, %State{state | moves: state.moves+1})
  end

  def handle_answer("l", guess, state), do: guess(%State{state | max: guess+1})
  def handle_answer("h", guess, state), do: guess(%State{state | max: guess-1})
  def handle_answer("y", _guess, %{moves: moves}) do
    IO.puts "I won in #{moves}"
  end
end
